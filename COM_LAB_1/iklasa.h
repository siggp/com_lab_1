#include<windows.h>

DEFINE_GUID(IID_IKlasa, 0xXXXXXXXX, 0xXXXX, 0xXXXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX);
DEFINE_GUID(CLSID_Klasa, 0xXXXXXXXX, 0xXXXX, 0xXXXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX);

class IKlasa: public IUnknown {
public:
	virtual HRESULT STDMETHODCALLTYPE Push(int val) = 0;	// metody abstrakcyjne!
	virtual HRESULT STDMETHODCALLTYPE PopEx(int *val) = 0;
	virtual int STDMETHODCALLTYPE Pop() = 0;
};
