#include<windows.h>
#include"klasa.h"

extern volatile int usageCount;

Klasa::Klasa() {
	this->next = NULL;
	this->value = NULL;
	/* OK
	Tutaj dodaj inicjalizacj� warto�ci zmiennych stosu.
	*/
	usageCount++;
};


Klasa::~Klasa() {
	usageCount--;
	};


ULONG STDMETHODCALLTYPE Klasa::AddRef() {
	InterlockedIncrement(&m_ref);
	return m_ref;
	/* OK
	Tutaj zaimplementuj dodawanie referencji na obiekt.
	 */
};


ULONG STDMETHODCALLTYPE Klasa::Release() {
	ULONG rv = InterlockedDecrement(&m_ref);
	if (rv == 0) delete this;
	return rv;
	/* OK
	Tutaj zaimplementuj usuwania referencji na obiekt.
	Je�eli nie istnieje �adna referencja obiekt powinien zosta� usuni�ty.
	 */
};


HRESULT STDMETHODCALLTYPE Klasa::QueryInterface(REFIID iid, void **ptr) {
	if(ptr == NULL) return E_POINTER;
	if(IsBadWritePtr(ptr, sizeof(void *))) return E_POINTER;
	*ptr = NULL;
	if(iid == IID_IUnknown) *ptr = this;
	if(iid == IID_IKlasa) *ptr = this;
	if(*ptr != NULL) { AddRef(); return S_OK; };
	return E_NOINTERFACE;
	};


HRESULT STDMETHODCALLTYPE Klasa::Push(int val) {
	Klasa *x = new Klasa();
	x->value = this->value;
	x->next = this->next;
	this->value = val;
	this->next = x;

	if (this->next != NULL) {
		return S_OK;
	}
	else {
		return E_FAIL;
	}
	/* OK
	Tutaj zaimplementuj operacj� odk�adania na stos.
	Je�eli operacja udana zwr�� S_OK je�li nie zwr�� E_FAIL.
	 */
};


HRESULT STDMETHODCALLTYPE Klasa::PopEx(int *val) {
	if (this->value == NULL) {
		return E_FAIL;
	}
	else {
		int newValue = this->next->value;
		this->next = this->next->next;
		this->value = newValue;
		return S_OK;
	}
	/* OK
	Tutaj zaimplementuj operacj� pobierania ze stosu.
	Je�eli operacja udana zwr�� S_OK je�li nie zwr�� E_FAIL.
	 */
};


int STDMETHODCALLTYPE Klasa::Pop() {
	if (this->value == NULL) {
		return -1;
	}
	else {
		int toReturn = this->value;
		int newValue = this->next->value;
		this->next = this->next->next;
		this->value = newValue;
		return toReturn;
	}
	/* OK
	Tutaj zaimplementuj operacj� pobierania ze stosu.
	Je�eli operacja udana zwr�� pobran� warto�� je�li nie zwr�� -1.
	 */
};
